from .vggish import vggish_postprocess
from .vggish import vggish_params
from .vggish import mel_features
from .vggish import vggish_slim
import numpy as np
import resampy
import tensorflow as tf

MODEL_PATH = 'audio_extractor/model/vggish_model.ckpt'
PAC_PARAM_PATH = 'audio_extractor/model/vggish_pca_params.npz'

class VGGishFeatureExtractor(object):

    def __init__(self, model_path=MODEL_PATH, pac_param_path=PAC_PARAM_PATH):
        self.model_path = model_path
        self.pac_param_path = pac_param_path
        self.graph = tf.Graph()
        with self.graph.as_default():
            self.session = tf.Session()
            self.load_VGGish_model()
            self.features_tensor = self.session.graph.get_tensor_by_name(
                vggish_params.INPUT_TENSOR_NAME)
            self.embedding_tensor = self.session.graph.get_tensor_by_name(
                vggish_params.OUTPUT_TENSOR_NAME)
        self.load_pca_model()

    def extract_audio_feature(self, data, sample_rate, apply_pac=True):
        """
        extract the audio features from data by model VGGish

        :param data: audio data
        :param sample_rate: the sampling rate of the audio
        :param apply_pac: (bool)
        :return: audio feature which dimension is [the length(sec) of audio, 128 ]
        """
        input_batch = self.waveform_to_examples(data, sample_rate)

        [embedding_batch] = self.session.run([self.embedding_tensor],
                                     feed_dict={self.features_tensor: input_batch})
        if apply_pac:
            embedding_batch = self.apply_pca(embedding_batch)

        self.embedding_batch = embedding_batch
        return embedding_batch

    def waveform_to_examples(self, data, sample_rate):
        """Converts audio waveform into an array of examples for VGGish.

        Args:
          data: np.array of either one dimension (mono) or two dimensions
            (multi-channel, with the outer dimension representing channels).
            Each sample is generally expected to lie in the range [-1.0, +1.0],
            although this is not required.
          sample_rate: Sample rate of data.

        Returns:
          3-D np.array of shape [num_examples, num_frames, num_bands] which represents
          a sequence of examples, each of which contains a patch of log mel
          spectrogram, covering num_frames frames of audio and num_bands mel frequency
          bands, where the frame length is vggish_params.STFT_HOP_LENGTH_SECONDS.
        """
        # Convert to mono.
        if len(data.shape) > 1:
            data = np.mean(data, axis=1)
        # Resample to the rate assumed by VGGish.
        if sample_rate != vggish_params.SAMPLE_RATE:
            data = resampy.resample(data, sample_rate, vggish_params.SAMPLE_RATE)

        # Compute log mel spectrogram frames_extractor.
        log_mel = mel_features.log_mel_spectrogram(
            data,
            audio_sample_rate=vggish_params.SAMPLE_RATE,
            log_offset=vggish_params.LOG_OFFSET,
            window_length_secs=vggish_params.STFT_WINDOW_LENGTH_SECONDS,
            hop_length_secs=vggish_params.STFT_HOP_LENGTH_SECONDS,
            num_mel_bins=vggish_params.NUM_MEL_BINS,
            lower_edge_hertz=vggish_params.MEL_MIN_HZ,
            upper_edge_hertz=vggish_params.MEL_MAX_HZ)

        # Frame frames_extractor into examples.
        features_sample_rate = 1.0 / vggish_params.STFT_HOP_LENGTH_SECONDS
        example_window_length = int(round(
            vggish_params.EXAMPLE_WINDOW_SECONDS * features_sample_rate))
        example_hop_length = int(round(
            vggish_params.EXAMPLE_HOP_SECONDS * features_sample_rate))
        log_mel_examples = mel_features.frame(
            log_mel,
            window_length=example_window_length,
            hop_length=example_hop_length)
        return log_mel_examples

    def apply_pca(self, embedding_batch):
        postprocessed_batch = self.pproc.postprocess(embedding_batch)
        return postprocessed_batch

    def load_VGGish_model(self):
        vggish_slim.define_vggish_slim()
        vggish_slim.load_vggish_slim_checkpoint(self.session, self.model_path)

    def load_pca_model(self):
        self.pproc = vggish_postprocess.Postprocessor(self.pac_param_path)






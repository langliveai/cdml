import os
import fnmatch
import numpy as np

class VirtualDataSet(object):

    def __init__(self,config):
        self.create_vurtaul_data_set(config)

    def create_vurtaul_data_set(self, config):
        for root, dirs, v_files in os.walk(config.video_dir):
            pattern = '*.mp4'
            data = []
            data_set = []
            i = 0
            count = 0
            for v_file in v_files:
                if fnmatch.fnmatch(v_file, pattern):
                    video_file = os.path.join(config.video_dir, v_file)
                    data.append(video_file)
                    i = i + 1
                    if i % 3 == 0:
                        data_set.append(data)
                        count = count + 1
                        data = []
                        i = 0

        self.data_set = data_set
        self.num_batches = int(np.ceil(count*1.0/config.batch_size))


class Config(object):
    def __init__(self):
        # about the model architecture
        self.video_feature_shape = [1024]
        self.audio_feature_shape = [128]
        # about the weight initialization and regularization

        # about loss
        self.loss_type = 'hinge'
        self.margin = 0.5

        # about the optimization
        self.num_epochs = 10000000
        self.batch_size = 3
        self.optimizer = 'Adam'  # 'Adam', 'RMSProp', 'Momentum' or 'SGD'
        self.initial_learning_rate = 0.00001
        self.learning_rate_decay_factor = 1.0
        self.num_steps_per_decay = 100000
        self.clip_gradients = 5.0
        self.momentum = 0.0
        self.use_nesterov = True  # for Momentum
        self.decay = 0.9  # for RMSProp
        self.centered = True  # for RMSProp
        self.beta1 = 0.9  # for Adam
        self.beta2 = 0.999  # for Adam
        self.epsilon = 1e-6

        # about path of data
        self.video_dir = './videos/'
        self.frames_model_path = './frames_extractor/model'
        self.audio_model_path = './audio_extractor/model/vggish_model.ckpt'
        self.audio_pac_path = './audio_extractor/model/vggish_pca_params.npz'
        self.model_save_dir = './models/'
        # about the saver
        self.save_period = 50

        # about the training

        # about the evaluation

        # about the testing
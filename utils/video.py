"""
Helper functions & Utilities used for Video Processing

Copyright 2018 Langlive All Rights Reserved.
"""
import os
import ntpath
import subprocess
import cv2
import numpy
import logging
import time
from pathlib import Path
import soundfile as sf

class VideoCapture:

    def __init__(self, video_file):
        """
        :param video_file: (str) video file path
        """
        self.mp4_file = video_file
        self.cap = cv2.VideoCapture(video_file)
        self.source_fps = self.cap.get(cv2.CAP_PROP_FPS)
        self.source_frame_count = int(self.cap.get(cv2.CAP_PROP_FRAME_COUNT))
        self.source_length = self.source_frame_count / self.source_fps

        self.logger = logging.getLogger("im_recsys.utils.video.VideoCapture")
        self.logger.info(f"echo")

    @staticmethod
    def get_target_frames_id(source_frame_count, target_frame_count):
        avg = source_frame_count / float(target_frame_count)
        target = []
        for _id in range(source_frame_count):
            if _id > avg * len(target):
                target.append(_id)
        return target

    def get_frames(self, target_fps=1, write_output=False, output_dir="./frame_captured",
                   output_prefix=None, debug=False):
        """
        Capture video frames, return numpy array and/or write images

        :param target_fps: (int) target fps, note that this is just an approximation since source fps is not integer
        :param write_output: (bool)
        :param output_dir: (str)
        :param output_prefix: (str)
        :param debug: (bool)
        :return:
        """

        frames = []
        if write_output:
            output_dir = Path(output_dir)
            output_dir.mkdir(parents=True, exist_ok=True)
            if output_prefix is None:
                output_prefix = f"cap-{time.time()}"

        target_frames_id = self.get_target_frames_id(source_frame_count=self.source_frame_count,
                                                     target_frame_count=int(self.source_length * target_fps))
        self.logger.debug(f"target_frames_id: {target_frames_id}")

        current_frame_id = 0
        while self.cap.isOpened():
            frame_id = self.cap.get(cv2.CAP_PROP_POS_FRAMES)
            if debug:
                self.logger.debug(f"current frame: {frame_id} / {self.source_frame_count}")

            retval, frame = self.cap.read()
            if retval is not True:
                break

            if frame_id in target_frames_id:
                if write_output:
                    cv2.imwrite(
                        str(output_dir.joinpath(f"{output_prefix}-{current_frame_id}.jpg")),
                        frame,
                    )
                    current_frame_id += 1
                frames.append(frame)
        self.cap.release()
        return numpy.array(frames)

    def convert_to_wav(self, output_dir="./wav_file", ab='160k', ac=2, sr=44100):
        """
        convert the video of mp3 type to wav

        :param output_dir: the path of output wav file
        :param ab: audio bitrate
        :param ac: number of audio channels
        :param sr: sampling rate
        :return:
        """

        output_dir = Path(output_dir)
        output_dir.mkdir(parents=True, exist_ok=True)
        head, tail = ntpath.split(self.mp4_file)
        filename, file_extension = os.path.splitext(tail)
        self.wav_file = str(output_dir) + '/' + filename+'.wav'
        cmd = f"ffmpeg -y -i {self.mp4_file} -ab {ab} -ac {ac} -ar {sr} -vn {self.wav_file}"
        subprocess.call(cmd, shell=True)

    def get_audio(self, output_dir="./wav_file", ab='160k', ac=2, sr=44100, normalization=True):
        """
        get the audio datas

        :param output_dir: the path of output wav file
        :param ab: audio bitrate
        :param ac: number of audio channels
        :param sr: sampling rate(default: 44100 points / 1 sec)
        :param normalization:(bool)
        :return: The datas, wav_data, sr, read from sf.read
        """
        self.convert_to_wav(output_dir, ab, ac, sr)
        wav_data, sr = sf.read(self.wav_file, dtype='int16')
        assert wav_data.dtype == numpy.int16, 'Bad sample type: %r' % wav_data.dtype
        if normalization == True:
            wav_data = wav_data / 32768.0  # Convert to [-1.0, +1.0]
        return wav_data, sr




from config import Config
from CDML import CDML_model
import tensorflow as tf
from dataset import VirtualDataSet



FLAGS = tf.app.flags.FLAGS

tf.flags.DEFINE_string('phase', 'train',
                       'The phase can be train, eval or test')


def main(argv):
    config = Config()
    config.phase = FLAGS.phase

    with tf.Session() as sess:
        if FLAGS.phase == 'train':
            data_set = VirtualDataSet(config)
            model = CDML_model(config)
            sess.run(tf.global_variables_initializer())
            model.train(sess, data_set)

        # if FLAGS.phase == 'test':
        # moedl.test(sess, data)






if __name__=='__main__':
    tf.app.run()
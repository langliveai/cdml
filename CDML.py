import numpy as np
from tqdm import tqdm
import tensorflow as tf
from frames_extractor.frame import YouTube8MFeatureExtractor
from audio_extractor.audio import VGGishFeatureExtractor
from utils.video import VideoCapture
import os
import pickle
import copy


class CDML_model(object):

    def __init__(self, config):
        self.config = config
        self.frames_model_path = config.frames_model_path
        self.audio_model_path = config.audio_model_path
        self.audio_pac_path = config.audio_pac_path
        self.video_feature_shape = config.video_feature_shape
        self.audio_feature_shape = config.audio_feature_shape
        self.global_step = tf.Variable(0, name='global_step', trainable=False)

        self.build_frames_model()
        self.build_audio_model()
        self.build_embedding_model()
        if config.phase == 'train':
            self.build_optimizer()

    def build_frames_model(self):
        self.frames_extractor = YouTube8MFeatureExtractor(self.frames_model_path)

    def build_audio_model(self):
        self.audio_extractor = VGGishFeatureExtractor(self.audio_model_path, self.audio_pac_path)

    def build_embedding_model(self):
        loss_type = self.config.loss_type
        margin = self.config.margin

        anchor_videos = tf.placeholder(
            dtype=tf.float32,
            shape=[self.config.batch_size] + self.video_feature_shape)

        anchor_audios = tf.placeholder(
            dtype=tf.float32,
            shape=[self.config.batch_size] + self.audio_feature_shape)

        positive_videos = tf.placeholder(
            dtype=tf.float32,
            shape=[self.config.batch_size] + self.video_feature_shape)

        positive_audios = tf.placeholder(
            dtype=tf.float32,
            shape=[self.config.batch_size] + self.audio_feature_shape)

        negative_videos = tf.placeholder(
            dtype=tf.float32,
            shape=[self.config.batch_size] + self.video_feature_shape)

        negative_audios = tf.placeholder(
            dtype=tf.float32,
            shape=[self.config.batch_size] + self.audio_feature_shape)

        anchor_vectors = tf.concat([anchor_videos, anchor_audios], 1)
        positive_vectors = tf.concat([positive_videos, positive_audios], 1)
        negative_vectors = tf.concat([negative_videos, negative_audios], 1)

        embd_anchor_vectors = self.embedding_model(anchor_vectors)
        embd_positive_vectors = self.embedding_model(positive_vectors)
        embd_negative_vectors = self.embedding_model(negative_vectors)

        def _hinge_loss(x):
            shape = tf.shape(x)
            zeros = tf.tile([0.0], [shape[0]])
            return tf.math.maximum(zeros, x)

        def _reduce_distance_square(embeddings):
            dot_product = tf.matmul(embeddings, tf.transpose(embeddings))
            distances_square = tf.diag_part(dot_product)
            return distances_square

        if loss_type == 'hinge':
            embd_anc_pos_dist = _reduce_distance_square(tf.subtract(embd_anchor_vectors, embd_positive_vectors))
            embd_anc_neg_dist = _reduce_distance_square(tf.subtract(embd_anchor_vectors, embd_negative_vectors))
            triple_dist = embd_anc_pos_dist - embd_anc_neg_dist + tf.tile([margin], [self.config.batch_size])
            self.loss = _hinge_loss(triple_dist)
            self.total_loss = tf.reduce_sum(self.loss)

            # Debug Information
            self.total_loss = tf.Print(self.total_loss, ['DBG', anchor_vectors.shape, embd_anchor_vectors.shape], summarize=100)
            self.total_loss = tf.Print(self.total_loss, ['DBG', embd_anc_pos_dist], summarize=100)
            self.total_loss = tf.Print(self.total_loss, ['DBG', embd_anc_neg_dist], summarize=100)
            self.total_loss = tf.Print(self.total_loss, ['DBG', triple_dist.shape], summarize=100)
            self.total_loss = tf.Print(self.total_loss, ['DBG', triple_dist, self.loss], summarize=100)
            self.total_loss = tf.Print(self.total_loss, ['DBG', self.total_loss], summarize=100)


        self.anchor_videos = anchor_videos
        self.anchor_audios = anchor_audios
        self.positive_videos = positive_videos
        self.positive_audios = positive_audios
        self.negative_videos = negative_videos
        self.negative_audios = negative_audios



    def embedding_model(self,
                        vec,
                        units=1024 + 128,
                        activation=tf.nn.relu,
                        use_bias=True,
                        trainable=True):

        fc_kernel_initializer = tf.random_uniform_initializer(
            minval=-1,
            maxval=1)

        with tf.variable_scope("embedding", reuse=tf.AUTO_REUSE):
            dense1 = tf.layers.dense(
                inputs=vec,
                units=units,
                activation=activation,
                use_bias=use_bias,
                trainable=trainable,
                kernel_initializer=fc_kernel_initializer,
                name='fc1')

            output = tf.layers.dense(
                inputs=dense1,
                units=units,
                activation=activation,
                use_bias=use_bias,
                trainable=trainable,
                kernel_initializer=fc_kernel_initializer,
                name='fc2')

            return output

    def build_optimizer(self):
        """ Setup the optimizer and training operation. """
        config = self.config
        learning_rate = tf.constant(config.initial_learning_rate)
        if config.learning_rate_decay_factor < 1.0:
            def _learning_rate_decay_fn(learning_rate, global_step):
                return tf.train.exponential_decay(
                    learning_rate,
                    global_step,
                    decay_steps=config.num_steps_per_decay,
                    decay_rate=config.learning_rate_decay_factor,
                    staircase=True)

            learning_rate_decay_fn = _learning_rate_decay_fn
        else:
            learning_rate_decay_fn = None

        with tf.variable_scope('optimizer', reuse=tf.AUTO_REUSE):
            if config.optimizer == 'Adam':
                optimizer = tf.train.AdamOptimizer(
                    learning_rate=config.initial_learning_rate,
                    beta1=config.beta1,
                    beta2=config.beta2,
                    epsilon=config.epsilon
                )
            elif config.optimizer == 'RMSProp':
                optimizer = tf.train.RMSPropOptimizer(
                    learning_rate=config.initial_learning_rate,
                    decay=config.decay,
                    momentum=config.momentum,
                    centered=config.centered,
                    epsilon=config.epsilon
                )
            elif config.optimizer == 'Momentum':
                optimizer = tf.train.MomentumOptimizer(
                    learning_rate=config.initial_learning_rate,
                    momentum=config.momentum,
                    use_nesterov=config.use_nesterov
                )
            else:
                optimizer = tf.train.GradientDescentOptimizer(
                    learning_rate=config.initial_learning_rate
                )

            opt_op = tf.contrib.layers.optimize_loss(
                loss=self.total_loss,
                global_step=self.global_step,
                learning_rate=learning_rate,
                optimizer=optimizer,
                clip_gradients=config.clip_gradients,
                learning_rate_decay_fn=learning_rate_decay_fn)

        self.opt_op = opt_op

    def build_summary(self):
        """ Build the summary (for TensorBoard visualization). """
        with tf.name_scope("variables"):
            for var in tf.trainable_variables():
                with tf.name_scope(var.name[:var.name.find(":")]):
                    self.variable_summary(var)

        with tf.name_scope("metrics"):
            tf.summary.scalar("total_loss", self.total_loss)

        self.summary = tf.summary.merge_all()

    def audio_extract(self, wav_data, sampling_rate, apply_pca=True, method='L2'):
        """return aggregate feature of audio in L2 norm"""
        audio_feature = self.audio_extractor.extract_audio_feature(wav_data, sampling_rate, apply_pca)
        if method == 'L2':
            return np.linalg.norm(audio_feature, axis=0)
        elif method == 'mean':
            return np.mean(audio_feature, axis=0)

    def video_extract(self, frames_rgb, apply_pca=True, method='L2'):
        """return aggregate feature of frames in L2 norm."""
        frames_v = []
        for frame_rgb in frames_rgb:
            frame_v = self.frames_extractor.extract_rgb_frame_features(frame_rgb, apply_pca)
            frames_v.append(frame_v)

        if method == 'L2':
            return np.linalg.norm(frames_v, axis=0)
        elif method == 'mean':
            return np.mean(frames_v, axis=0)

    def train(self, sess, datas):

        config = self.config

        def extract(data):
            vc = VideoCapture(data)
            frames = vc.get_frames()
            wav, sr = vc.get_audio(ac=1)
            video_feature = self.video_extract(frames)
            audio_feature = self.audio_extract(wav, sr)
            return video_feature, audio_feature

        a_video_features = []
        a_audio_features = []
        p_video_features = []
        p_audio_features = []
        n_video_features = []
        n_audio_features = []
        for data in datas.data_set:
            anchor = data[0]
            positive = data[1]
            negative = data[2]
            a_video_feature, a_audio_feature = extract(anchor)
            p_video_feature, p_audio_feature = extract(positive)
            n_video_feature, n_audio_feature = extract(negative)
            a_video_features.append(a_video_feature)
            a_audio_features.append(a_audio_feature)
            p_video_features.append(p_video_feature)
            p_audio_features.append(p_audio_feature)
            n_video_features.append(n_video_feature)
            n_audio_features.append(n_audio_feature)


        for _ in tqdm(list(range(config.num_epochs)), desc='epoch'):
            loss_list = []
            for _ in tqdm(list(range(datas.num_batches)), desc='batch'):
                feed_dict = {self.anchor_videos: a_video_features,
                             self.anchor_audios: a_audio_features,
                             self.positive_videos: p_video_features,
                             self.positive_audios: p_audio_features,
                             self.negative_videos: n_video_features,
                             self.negative_audios: n_audio_features}
                _, loss, global_step = sess.run([self.opt_op, self.total_loss, self.global_step], feed_dict=feed_dict)
                loss_list.append(loss)

                if (global_step + 1) % config.save_period == 0:
                    self.save()

            print('\nloss: %f' % np.mean(loss_list))
            print('\nsteps: %f' % global_step)
        self.save()
        print("Training complete.")

    def test(self, sess, data):
        pass

    def save(self):
        """ Save the model """
        config = self.config
        data = {v.name: v.eval() for v in tf.global_variables()}
        save_path = os.path.join(config.model_save_dir, str(self.global_step.eval()))
        print((" Saving the model to %s..." % (save_path+".npy")))
        np.save(save_path, data)
        info_file = open(os.path.join(config.model_save_dir, "config.pickle"), "wb")
        config_ = copy.copy(config)
        config_.global_step = self.global_step.eval()
        pickle.dump(config_, info_file)
        info_file.close()
        print("Model saved.")

    def load(self, sess, model_file=None):
        """ Load the model """
        config = self.config
        if model_file is not None:
            save_path = model_file
        else:
            info_path = os.path.join(config.model_save_dir, "config.pickle")
            info_file = open(info_path, "rb")
            config = pickle.load(info_file)
            global_step = config.global_step
            info_file.close()
            save_path = os.path.join(config.model_save_dir,
                                     str(global_step)+".npy")

        print("Loading the model from %s..." %save_path)
        data_dict = np.load(save_path, encoding='latin1').item()
        count = 0
        for v in tqdm(tf.global_variables()):
            if v.name in data_dict.keys():
                sess.run(v.assign(data_dict[v.name]))
                count += 1
        print("%d tensors loaded." %count)








